# inicio

**Nodo Ambiental**

**_Información y nexos para la preservación del ambiente y el desarrollo sostenible_**

**Nodo Ambiental** es una organización que colabora con la sistematización de información y con redes de cooperación para la preservación del ambiente y el desarrollo sostenible.

**Nodo Ambiental** es una Organización de la Sociedad Civil (OSC), no gubernamental (ONG), dirigida a otras organizaciones, escuelas y profesionales.

# buscamos

- 1 Colaborar en el desarrollo y ejecución de proyectos de acción socio-ambiental.

- 2 Difundir proyectos de salud ambiental, incluyendo aspectos sociales, educativos y lúdicos.

- 3 Desarrollar repositorios de información verificable y reproducible.
